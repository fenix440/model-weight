<?php

use Fenix440\Model\Weight\Interfaces\WeightAware;
use Fenix440\Model\Weight\Traits\WeightTrait;

/**
 * Class WeightTraitTest
 *
 * @coversDefaultClass Fenix440\Model\Weight\Traits\WeightTrait
 *
 * @author Bartlomiej Szala <fenix440@gmail.com>
 */
class WeightTraitTest extends \Codeception\TestCase\Test
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }


    /******************************************************************************
     * Providers
     *****************************************************************************/

    /**
     * Get mock for given trait
     * @return PHPUnit_Framework_MockObject_MockObject|Fenix440\Model\Weight\Interfaces\WeightAware
     */
    protected function getTraitMock()
    {
        $m = $this->getMockForTrait('Fenix440\Model\Weight\Traits\WeightTrait');
        return $m;
    }

    /**
     * Get a dummy object of that trait
     * @return DummyWeight
     */
    protected function getDummyObj(){
        return new DummyWeight();
    }

    /******************************************************************************
     * Tests
     *****************************************************************************/



    /**
     * @test
     * @covers  ::getDefaultWeight
     * @covers  ::setWeight
     * @covers  ::isWeightValid
     * @covers  ::getWeight
     * @covers  ::hasWeight
     */
    public function getDefaultWeight()
    {
        $trait = $this->getTraitMock();
        $this->assertNull($trait->getWeight());
    }

    /**
     * @test
     * @covers  ::setWeight
     * @covers  ::getWeight
     * @covers  ::isWeightValid
     * @covers  ::hasWeight
     * @covers  ::hasDefaultWeight
     */
    public function setAndGetWeight()
    {
        $trait = $this->getTraitMock();
        $weight = 12.3;
        $trait->setWeight($weight);

        $this->assertSame($weight, $trait->getWeight());
    }


    /**
     * @test
     * @covers  ::setWeight
     * @covers  ::getWeight
     * @covers  ::isWeightValid
     * @covers  ::hasWeight
     * @covers  ::hasDefaultWeight
     */
    public function setAndGetIntegerWeight()
    {
        $trait = $this->getTraitMock();
        $weight = 12;
        $trait->setWeight($weight);

        $this->assertSame($weight, $trait->getWeight());
    }

    /**
     * @test
     * @covers  ::setWeight
     * @covers  ::isWeightValid
     * @expectedException \Fenix440\Model\Weight\Exceptions\InvalidWeightException
     */
    public function setInvalidWeight()
    {
        $trait = $this->getTraitMock();
        $weight = 0;

        $trait->setWeight($weight);
    }

    /**
     * @test
     *
     * @covers ::setWeight
     * @covers ::getWeight
     * @covers ::isWeightValid
     * @covers ::hasWeight
     * @covers ::hasDefaultWeight
     * @covers ::getDefaultWeight
     */
    public function getWeightFromCustomDefault(){
        $dummy = $this->getDummyObj();
        $dummy->defaultWeight=92.00;

        $this->assertSame($dummy->defaultWeight, $dummy->getWeight());
    }


}


/**
 * Class DummyWeight
 *
 */
class DummyWeight
{

    use WeightTrait;

    public $defaultWeight=null;

    public function getDefaultWeight(){
        return $this->defaultWeight;
    }
}