<?php namespace Fenix440\Model\Weight\Traits;

use Aedart\Validate\Number\FloatValidator;
use Fenix440\Model\Weight\Exceptions\InvalidWeightException;

/**
 * Trait WeightTrait
 *
 * @see WeightAware
 *
 * @package      Fenix440\Model\Weight\Traits
 * @author      Bartlomiej Szala <fenix440@gmail.com>
 */
trait WeightTrait
{

    /**
     * Weight for a given component
     * @var null|float
     */
    protected $weight=null;

    /**
     * Set weight for given component
     *
     * @param float $weight    Weight
     * @return void
     * @throws InvalidWeightException If given weight is invalid
     */
    public function setWeight($weight){
        if(!$this->isWeightValid((float)$weight))
            throw new InvalidWeightException(sprintf('Weight: "%d" is invalid',$weight));
        $this->weight=$weight;
    }

    /**
     * Get weight for given component
     *
     * @see WeightAware::getDefaultWeight()
     * @see WeightAware::setWeight($weight)
     *
     * @return float|null
     */
    public function getWeight(){
        if(!$this->hasWeight() && $this->hasDefaultWeight())
            $this->setWeight($this->getDefaultWeight());
        return $this->weight;
    }

    /**
     * Validates if this component weight is valid
     *
     * @param mixed $weight      Component weight
     * @return bool             true/false
     */
    public function isWeightValid($weight){
        return FloatValidator::isValid($weight,array(FloatValidator::MAX_PRECISION_RANGE => 2)) && $weight > 0;
    }

    /**
     * Get this component default weight
     *
     * @return float|null
     */
    public function getDefaultWeight(){
        return null;
    }

    /**
     * Check if this component has set a weight
     * @return bool                     true/false
     */
    public function hasWeight(){
        return (!is_null($this->weight))? true:false;
    }

    /**
     * Checks if this component has set a default weight
     * @return bool                     true/false
     */
    public function hasDefaultWeight(){
        return (!is_null($this->getDefaultWeight()))? true:false;
    }

}