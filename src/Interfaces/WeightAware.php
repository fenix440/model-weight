<?php namespace Fenix440\Model\Weight\Interfaces;
use Fenix440\Model\Weight\Exceptions\InvalidWeightException;

/**
 * Interface WeightAware
 *
 * A component/resource must be aware of "Weight".
 * Provides an option to set, get and validate weight for
 * given component.
 *
 * Furthermore, depending upon implementation, a default value might be returned, if no value has been set prior to obtaining it.
 *
 * @author Bartlomiej Szala <fenix440@gmail.com>
 * @package      Fenix440\Model\Weight\Interfaces
 */
interface WeightAware{

    /**
     * Set weight for given component
     *
     * @param float $weight    Weight
     * @return void
     * @throws InvalidWeightException If given weight is invalid
     */
    public function setWeight($weight);

    /**
     * Get weight for given component
     *
     * @see WeightAware::getDefaultWeight()
     * @see WeightAware::setWeight($weight)
     *
     * @return float|null
     */
    public function getWeight();

    /**
     * Validates if this component weight is valid
     *
     * @param mixed $weight      Component weight
     * @return bool             true/false
     */
    public function isWeightValid($weight);

    /**
     * Get this component default weight
     *
     * @return float|null
     */
    public function getDefaultWeight();

    /**
     * Check if this component has set a weight
     * @return bool                     true/false
     */
    public function hasWeight();

    /**
     * Checks if this component has set a default weight
     * @return bool                     true/false
     */
    public function hasDefaultWeight();
}