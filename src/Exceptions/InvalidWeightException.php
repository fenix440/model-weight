<?php namespace Fenix440\Model\Weight\Exceptions;
/**
 * Class InvalidWeightException
 *
 * Throws this exception when invalid weight has been provided
 *
 * @package Fenix440\Model\Weight\Exceptions
 * @author      Bartlomiej Szala <fenix440@gmail.com>
 */
class InvalidWeightException extends \InvalidArgumentException
{


}

 